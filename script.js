// JS — однопоточна мова, тобто все відбувається крок за кроком, рядок за рядком. Але є командами, які виконуються тривалий час і можуть блокувати увесь процес
// Асинхронний код прибирає блокуючу команду з потоку, тепер вона буде працювати в иншому місці. А код продовжить працювати далі

const button = document.querySelector('.btn')

button.addEventListener('click', getInfo)



async function getInfo(){
    try {
        const response = await fetch('https://api.ipify.org/?format=json')
	    const getIp = await response.json()


        const responce = await fetch(`http://ip-api.com/json/${getIp.ip}`)
        const data = await responce.json()

        const {continent, country, regionName, city, district} = data

        const values = Object.values({continent, country, regionName, city, district})
        const keys = ['Continent', 'Country', 'Region', 'City', 'District']
        
        const div = document.querySelector('div') 
        const list = document.createElement('ul')

        
        values.forEach((value, i)=> {
           
            if (value === undefined) {
                list.innerHTML += `<li>${keys[i]}: Information not found</li>`
            } else {
                list.innerHTML +=  `<li>${keys[i]}: ${value}</li>`
            }

            div.append(list)
        });
    button.disabled = true

    } catch (error) {
        
    }
}



